JParrot
--------------------------------------

This is a simple tweet analysis tool.

Five main tools are currently available:
- geolocalized tweet search
- word cloud
- sentiment analysis
- Tweet streaming and analisys
- Twitter Competition and trivia management



Documentation
--------------------------------------

Documentation generated using [jsdoc](https://jsdoc.app).

Generate using:

```bash
jsdoc -c jsdoc_conf.js
```

This will create a documentation/ folder inside the project folder.
